<?php 

namespace App\Models;

use App\Core\Model;

class Product extends Model {

    public static $tableName = 'products';

    public static $fields = [

        'id' => 'index',

        'article' => [
            'type' => 'string',
            'size' => 16,
            'required' => true,
        ],

        'name' => [
            'type' => 'string',
            'size' => 255,
            'required' => true,
        ],

        'description' => [
            'type' => 'string',
            'size' => 255,            
        ],

        'weight' => [
            'type' => 'int',
        ],

        'price' => [
            'type' => 'int',
            'required' => true,
        ]

    ];

}
