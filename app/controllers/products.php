<?php

namespace App\Controllers;

use App\Core\Controller;
use App\Core\Response;
use App\Models\Product;

class Products extends Controller {

    public function create($payload){

        if (!$payload){
            Response::error('Пустой запрос');
        }
        
        $product = Product::createFromArray($payload);

        $error = $product->getValidationError();
        if ($error){
            Response::error($error);
        }
        
        $product->save();

        Response::success([
            'result' => [
                'id' => $product->id
            ]
        ]);

    }

    public function read($id){
        
        $product = $this->getProduct($id);
        
        Response::create()->setData($product->getPayload())->send();

    }

    public function update($id, $payload){
        
        if (!$payload){
            Response::error('Пустой запрос');
        }

        if (isset($payload['id'])){
            Response::error('ID товара нужно передать в URL');
        }

        foreach($payload as $key => $value){
            if (!isset(Product::$fields[$key])){
                $fieldNames = join(', ', (array_filter(array_keys(Product::$fields), function($name){
                    return $name != 'id';
                })));
                Response::error("Неизвестное поле: {$key}. Поддерживаемые поля: {$fieldNames}");
            }
        }        

        $product = $this->getProduct($id);
        $product->updateFromArray($payload);

        $error = $product->getValidationError();
        if ($error){
            Response::error($error);
        }
        
        $product->save();
        $product->reload();

        Response::success([
            'result' => $product->getPayload()
        ]);

    }

    public function delete($id){
        $product = $this->getProduct($id);
        $product->delete();
        Response::success();
    }

    private function getProduct($id){
        $product = Product::getById($id);
        if (!$product){            
            Response::error404('Товар не найден');
        }
        return $product;
    }
    
}
