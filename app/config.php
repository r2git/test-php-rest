<?php 
return [
    'db_type' => 'mysql',
    'db_host' => 'localhost',
    'db_name' => 'test_php_rest',
    'db_user' => 'root',
    'db_pass' => '',
    'db_charset' => 'utf8mb4'
];
