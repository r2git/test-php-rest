<?php

namespace App\Core;

class Config {

    private static $data = [];

    public static function get($key, $default=false){
        if (!self::$data) { self::load(); }
        return isset(self::$data[$key]) ? self::$data[$key] : $default;
    }

    public static function is($key){
        return !empty(self::$data[$key]);
    }

    private static function load(){
        self::$data = include_once ROOT_PATH . '/app/config.php';
    }

}
