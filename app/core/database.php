<?php

namespace App\Core;

class Database {

    private static $pdo;

    public static function init(){

        $type = Config::get('db_type');
        $host = Config::get('db_host');
        $name = Config::get('db_name');
        $user = Config::get('db_user');
        $pass = Config::get('db_pass');
        $charset = Config::get('db_charset');

        $dsn = "{$type}:host={$host};dbname={$name};charset={$charset}";

        $options = [
            \PDO::ATTR_ERRMODE => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC
        ];

        try {
            self::$pdo = new \PDO($dsn, $user, $pass, $options);
        } catch (\PDOException $e) {
            throw new \Exception("Ошибка подключения к базе данных: {$e->getMessage()}");
        }

    }

    public static function query($query, $params = []){

        if (!$params) {
            return self::$pdo->query($query);
        }

        $pdoQuery = self::$pdo->prepare($query, $params);
        $pdoQuery->execute($params);

        return $pdoQuery;

    }

    public static function insert($tableName, $values){

        $columnNames = join(',', array_keys($values));

        $columnValues = array_map(function($value){
            return $value ? $value : null;
        }, $values);

        $columnPlaceholders = join(',', array_map(function($value){
            return '?';
        }, $values));        

        $sql = "INSERT INTO {$tableName} ({$columnNames}) VALUES ({$columnPlaceholders})";        

        self::query($sql, array_values($columnValues));

        return self::$pdo->lastInsertId();

    }

    public static function select($tableName, $conditions = [], $limit = false){

        $results = [];
        $sql = "SELECT * FROM {$tableName}";

        if ($conditions){
            $where = self::convertConditionsToWhere($conditions);
            $sql .= " WHERE {$where}";
        }

        if ($limit && is_numeric($limit)){
            $sql .= " LIMIT {$limit}";
        }

        $pdoQuery = self::query($sql, array_map(function($value){
            return $value ? $value : null;
        }, array_values($conditions)));

        foreach($pdoQuery as $row){
            $results[] = $row;
        }

        return $results;
        
    }

    public static function delete($tableName, $conditions){

        $sql = "DELETE FROM {$tableName}";

        $where = self::convertConditionsToWhere($conditions);
    
        if ($conditions){
            $where = self::convertConditionsToWhere($conditions);
            $sql .= " WHERE {$where}";
        }

        self::query($sql, array_map(function($value){
            return $value ? $value : null;
        }, array_values($conditions)));

    }

    public static function update($tableName, $values, $conditions) {
        
        $columnValues = self::convertValuesToUpdateSet($values);

        $sql = "UPDATE {$tableName} SET {$columnValues}";

        if ($conditions){
            $where = self::convertConditionsToWhere($conditions);
            $sql .= " WHERE {$where}";
        }

        $queryValues = array_values($values);
        array_push($queryValues, ...array_values($conditions));

        self::query($sql, array_map(function($value){
            return $value ? $value : null;
        }, array_values($queryValues)));

    }

    private static function convertConditionsToWhere($conditions){
        $where = [];
        foreach(array_keys($conditions) as $fieldName){
            $where[] = "{$fieldName} = ?";
        }
        return join(' AND ', $where);
    }

    private static function convertValuesToUpdateSet($values){
        $set = [];
        foreach(array_keys($values) as $fieldName){
            $set[] = "{$fieldName} = ?";
        }
        return join(', ', $set);
    }

}
