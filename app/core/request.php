<?php

namespace App\Core;

class Request {

    public static function getServerValue($key){
        if (!isset($_SERVER[$key])) { return null; }
        return filter_input(INPUT_SERVER, $key);
    }

    public static function getMethod(){
        return self::getServerValue('REQUEST_METHOD');
    }

    public static function getPayload(){
        
        $method = self::getMethod();
        $payload = [];

        if ($method == 'GET'){
            $payload = $_GET;
        }

        if ($method == 'POST'){
            $payload = $_POST;
        }

        if ($method == 'PUT'){
            $payload = json_decode(file_get_contents("php://input"), true); 
            if (json_last_error() != JSON_ERROR_NONE){
                throw new \Exception("Запрос содержит невалидный JSON");
            }
        }

        return $payload;
        
    }

}
