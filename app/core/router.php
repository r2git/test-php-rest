<?php

namespace App\Core;

class Router {

    private static $currentURI = '';

    public static function getCurrentURI(){
        return self::$currentURI;
    }

    public static function route(){

        $uri = trim(Request::getServerValue('REQUEST_URI'), '/');       

        if ($uri == ''){
            Response::error("См. инструкцию в readme.md");
        }

        if ($uri && !preg_match('/^([a-zA-Z]+)\/?([0-9]?)$/i', $uri)){
            Response::error404();
        }

        $uriSegments = explode('/', $uri);

        $controllerName = $uriSegments[0];
        $itemId = count($uriSegments) == 2 ? $uriSegments[1] : false;

        $controller = Controller::get($controllerName);

        if (!$controller){
            Response::error404();
        }

        self::executeController($controller, $itemId);

    }

    private static function executeController($controller, $itemId){        

        $method = Request::getMethod();
        $payload = Request::getPayload();        

        if (in_array($method, ['GET', 'PUT', 'DELETE']) && !$itemId){            
            Response::error('Не указан ID');
        }

        if ($method == 'POST' && $itemId){
            Response::error('Указан ID, который не требуется');
        }

        if ($method == 'GET'){
            return $controller->read($itemId);
        }

        if ($method == 'POST'){
            return $controller->create($payload);
        }

        if ($method == 'PUT'){
            return $controller->update($itemId, $payload);
        }

        if ($method == 'DELETE'){
            return $controller->delete($itemId);
        }

    }


}
