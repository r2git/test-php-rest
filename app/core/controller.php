<?php

namespace App\Core;

class Controller {

    public static function get($controllerName){
        $controllerFile = ROOT_PATH . "/app/controllers/{$controllerName}.php";        
        if (!file_exists($controllerFile)){ return false; }
        $controllerClass = "App\\Controllers\\{$controllerName}";        
        $controllerObject = new $controllerClass();
        return $controllerObject;
    }

    public function create($payload){

    }

    public function read($id){

    }

    public function update($id, $payload){
        
    }

    public function delete($id){
        
    }

}
