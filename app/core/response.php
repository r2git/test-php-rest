<?php

namespace App\Core;

class Response {

    private $code = 200;
    private $data;

    public static function create(){
        return new self();
    }

    public static function error404($message = false){
        $response = self::create()->setCode(404);
        if ($message){
            $response->setData(['error'=>$message]);
        }
        $response->send();
    }

    public static function error($message){        
        $response = self::create()->setCode(500);
        if ($message){
            $response->setData(['error'=>$message]);
        }
        $response->send();
    }

    public static function success($data = []){
        self::create()->setData(array_merge($data, ['success'=>true]))->send();
    }

    public function setCode($code){
        $this->code = $code;        
        return $this;
    }

    public function setData($data){
        $this->data = $data;
        return $this;
    }

    public function setView($viewName, $viewData = []){        
        $this->viewName = $viewName;
        $this->viewData = $viewData;
        return $this;
    }

    public function send(){
        $this->sendHeaders();
        $this->sendBody();
        exit(0);
    }

    private function sendHeader($header){
        header($header);
    }

    private function sendHeaders(){
        if ($this->code == 200){
            return;
        }
        switch($this->code){
            case 404: 
                $this->sendHeader("HTTP/1.0 404 Not Found");
                $this->sendHeader("HTTP/1.1 404 Not Found");
                break;
            case 500: 
                $this->sendHeader("HTTP/1.1 500 Internal Server Error");
                break;
        }
    }

    private function sendBody(){

        if (empty($this->data) && empty($this->viewName)){ 
            return;
        }

        if ($this->data){
            $this->sendHeader("Content-Type: application/json");
            echo json_encode($this->data, JSON_UNESCAPED_UNICODE);        
        }

    }

}
