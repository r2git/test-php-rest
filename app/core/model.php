<?php

namespace App\Core;

class Model {

    public static $tableName;

    public static $fields = [];

    public static function getById($id){

        $objects = Database::select(static::$tableName, ['id' => $id]);

        if (!$objects){
            return false;
        }

        return self::createFromArray($objects[0]);

    }

    public static function createFromArray($array){
        $modelClass = get_called_class();
        $model = new $modelClass();
        $model->updateFromArray($array);        
        return $model;
    }

    public function updateFromArray($array){
        foreach(static::$fields as $name => $field){
            if (array_key_exists($name, $array)){
                $this->{$name} = $array[$name];
            }
        }
    }

    public function getValidationError(){
        foreach(static::$fields as $name => $field){
            if ($field == 'index') { 
                continue; 
            }
            $value = empty($this->{$name}) ? null : $this->{$name};
            if (!empty($field['required']) && $value == null){
                return "Не заполнено обязательное поле: {$name}";                
            }
            if ($value !== null && $field['type'] == 'int' && !is_numeric($value)){
                return "Требуется числовое значение в поле: {$name}";
            }
            if ($value !== null && $field['type'] == 'string' && mb_strlen($value) > $field['size']){
                return "Слишком длинное значение в поле: {$name}";
            }
        }
        return false;
    }

    public function reload(){
        $this->updateFromArray(get_called_class()::getById($this->id)->getPayload());
    }

    public function getPayload(){
        $payload = [];
        foreach(static::$fields as $name => $field){
            $payload[$name] = isset($this->{$name}) ? $this->{$name} : null;
        }
        return $payload;
    }

    public function save(){
        if (empty($this->id)){
            $this->create();
        } else {
            $this->update();
        }
    }
    
    public function create(){
        $this->id = Database::insert(static::$tableName, $this->getPayload());
        return $this->id;
    }

    public function update(){
        $payload = $this->getPayload();
        unset($payload['id']);
        Database::update(static::$tableName, $payload, ['id' => $this->id]);
    }

    public function delete(){
        Database::delete(static::$tableName, ['id' => $this->id]);
    }    

}