<?php 
    define('ROOT_PATH', __DIR__);
    require_once ROOT_PATH . "/app/autoload.php";

    try {
        App\Core\Database::init();
        App\Core\Router::route();
    } catch (Exception $e) {
        App\Core\Response::error($e->getMessage());
    }
